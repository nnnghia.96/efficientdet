import os
import torch
import numpy as np
from torch.utils.data import Dataset
import cv2
import json
import yaml

class Loaddata(Dataset):
    def __init__(self, root_dir, set='train2017', transform=None):
        self.root_dir = root_dir
        self.set_name = set
        self.transform = transform
        self.image_ids = self.load_image_id()
        self.load_classes()

    def load_image_id(self):
        path_train = os.path.join(self.root_dir, 'image')
        list_data = os.listdir(path_train)
        for i in range(len(list_data)):
            list_data[i] = os.path.splitext(list_data[i])[0]
        return list_data

    def load_classes(self):
        project_file = 'projects/shape.yml'
        info = yaml.safe_load(open(project_file).read())
        self.classes = {}
        list_objects = info['obj_list']
        for i in range(len(list_objects)):
            self.classes.update({list_objects[i] : i})
        self.labels = {}
        for key, val in self.classes.items():
            self.labels[val] = key
        
    # get length of list of image
    def __len__(self):
        return len(self.image_ids)

    # load an image by index
    def load_image(self, image_idx):
        image_file = os.path.join(self.root_dir, 'image', self.image_ids[image_idx] + '.jpg')
        img = cv2.imread(image_file)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        return img.astype(np.float32) / 255.

    # load annotation of an image by index
    def load_annotations(self, image_idx):
        annotations_file = os.path.join(self.root_dir, 'annotations' ,self.image_ids[image_idx] + '.json')
        with open(annotations_file) as json_file:
            data = json.load(json_file)
        annotations = np.zeros((0,5))
        for i in range(len(data['annotations'])):
            name = data['annotations'][i]['category_id']
            # skip annotations with no width, height
            if data['annotations'][i]['bbox'][2]<1 or data['annotations'][i]['bbox'][3]<1:
                continue
            annotation = np.zeros((1,5))
            annotation[0,:4] =  data['annotations'][i]['bbox']
            annotation[0,4] = self.classes[name]
            annotations = np.append(annotations, annotation, axis = 0)
        # transform from [x, y, w, h] to [x1, y1, x2, y2]
        annotations[:, 2] = annotations[:, 0] + annotations[:, 2]
        annotations[:, 3] = annotations[:, 1] + annotations[:, 3]
        return annotations

    def __getitem__(self, idx):
        img = self.load_image(idx)
        annot = self.load_annotations(idx)
        sample = {'img': img, 'annot': annot}
        if self.transform:
            sample = self.transform(sample)
        return sample

def collater(data):
    imgs = [s['img'] for s in data]
    annots = [s['annot'] for s in data]
    scales = [s['scale'] for s in data]

    imgs = torch.from_numpy(np.stack(imgs, axis=0))

    max_num_annots = max(annot.shape[0] for annot in annots)

    if max_num_annots > 0:

        annot_padded = torch.ones((len(annots), max_num_annots, 5)) * -1

        for idx, annot in enumerate(annots):
            if annot.shape[0] > 0:
                annot_padded[idx, :annot.shape[0], :] = annot
    else:
        annot_padded = torch.ones((len(annots), 1, 5)) * -1

    imgs = imgs.permute(0, 3, 1, 2)

    return {'img': imgs, 'annot': annot_padded, 'scale': scales}


class Resizer(object):
    """Convert ndarrays in sample to Tensors."""
    
    def __init__(self, img_size=512):
        self.img_size = img_size

    def __call__(self, sample):
        image, annots = sample['img'], sample['annot']
        height, width, _ = image.shape
        if height > width:
            scale = self.img_size / height
            resized_height = self.img_size
            resized_width = int(width * scale)
        else:
            scale = self.img_size / width
            resized_height = int(height * scale)
            resized_width = self.img_size

        image = cv2.resize(image, (resized_width, resized_height), interpolation=cv2.INTER_LINEAR)

        new_image = np.zeros((self.img_size, self.img_size, 3))
        new_image[0:resized_height, 0:resized_width] = image

        annots[:, :4] *= scale

        return {'img': torch.from_numpy(new_image).to(torch.float32), 'annot': torch.from_numpy(annots), 'scale': scale}


class Augmenter(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample, flip_x=0.5):
        if np.random.rand() < flip_x:
            image, annots = sample['img'], sample['annot']
            image = image[:, ::-1, :]

            rows, cols, channels = image.shape

            x1 = annots[:, 0].copy()
            x2 = annots[:, 2].copy()

            x_tmp = x1.copy()

            annots[:, 0] = cols - x2
            annots[:, 2] = cols - x_tmp

            sample = {'img': image, 'annot': annots}

        return sample


class Normalizer(object):

    def __init__(self, mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
        self.mean = np.array([[mean]])
        self.std = np.array([[std]])

    def __call__(self, sample):
        image, annots = sample['img'], sample['annot']

        return {'img': ((image.astype(np.float32) - self.mean) / self.std), 'annot': annots}
